import os
import time
import datetime
from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application


def get_db_creds():
	db = os.environ.get("DB", None) or os.environ.get("database", None)
	username = os.environ.get("USER", None) or os.environ.get("username", None)
	password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
	hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
	return db, username, password, hostname


def create_table():
	# Check if table exists or not. Create and populate it only if it does not exist.
	db, username, password, hostname = get_db_creds()
	table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT NOT NULL, title TEXT NOT NULL, director TEXT NOT NULL, actor TEXT NOT NULL, release_date TEXT NOT NULL, rating FLOAT NOT NULL, PRIMARY KEY (id))'

	cnx = ''
	try:
		cnx = mysql.connector.connect(user=username, password=password,
									  host=hostname,
									  database=db)
	except Exception as exp:
		print(exp)

	cur = cnx.cursor()

	try:
		cur.execute(table_ddl)
		cnx.commit()
	except mysql.connector.Error as err:
		if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
			print("already exists.")
		else:
			print(err.msg)


def query_data():

	db, username, password, hostname = get_db_creds()

	print("Inside query_data")
	print("DB: %s" % db)
	print("Username: %s" % username)
	print("Password: %s" % password)
	print("Hostname: %s" % hostname)

	cnx = ''
	try:
		cnx = mysql.connector.connect(user=username, password=password,
									  host=hostname,
									  database=db)
	except Exception as exp:
		print(exp)

	cur = cnx.cursor()

	cur.execute("SELECT * FROM movies")
	entries = [dict(movie=row) for row in cur.fetchall()]
	return entries

try:
	print("---------" + time.strftime('%a %H:%M:%S'))
	print("Before create_table")
	create_table()
	print("After create_table")
except Exception as exp:
	print("Got exception %s" % exp)
	conn = None

def print_msg(msg):
	return render_template('index.html', message= msg)

def print_entry_msg(msg, ents):
	return render_template('index.html', message= msg, entries=ents)

@app.route('/add_movie', methods=['POST'])
def add_movie():
	print("add_movie request Received.")
	db, username, password, hostname = get_db_creds()
	ttl = request.form['title'].title()
	cnx = ''
	try:
		cnx = mysql.connector.connect(user=username, password=password,
									  host=hostname,
									  database=db)
	except Exception as exp:
		print(exp)
		import MySQLdb
		cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

	try:
		cur = cnx.cursor()
		cur.execute("SELECT * FROM movies WHERE title = '" + ttl + "'")
		entries = [dict(movie=row) for row in cur.fetchall()]
		if len(entries):
			return print_msg("Movie " + ttl + " could not be inserted - Movie already exists")
	except Exception as exp:
		return print_msg("Got exception: " + str(exp))

	yr = request.form['year']
	act = request.form['actor'].title()
	dr = request.form['director'].title()
	r_d = request.form['release_date']
	rt = request.form['rating']

	try:
		cur = cnx.cursor()
		temp = "INSERT INTO movies (year, title, actor, director, release_date, rating) values ('" + yr +  "','" + ttl + "','" + act + "','" + dr  + "','"+ r_d + "','" + rt + "')"
		cur.execute(temp)
		cnx.commit()
		return print_msg("Movie " + ttl + " inserted successfully")
	except Exception as exp:
		return print_msg("Got exception: " + str(exp))

@app.route('/update_movie', methods=['POST'])
def update_movie():
	print("update_movie request Received")

	db, username, password, hostname = get_db_creds()
	ttl = request.form['title'].title()
	cnx = ''
	try:
		cnx = mysql.connector.connect(user=username, password=password,
									  host=hostname,
									  database=db)
	except Exception as exp:
		print(exp)
		import MySQLdbc
		cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

	try:
		cur = cnx.cursor()
		cur.execute("SELECT * FROM movies WHERE title = '" + ttl + "'")
		entries = [dict(movie=row) for row in cur.fetchall()]
		if not len(entries):
			return print_msg("Movie " + ttl + " could not be updated - Movie does not exist")
	except Exception as exp:
		return print_msg("Got exception: " + str(exp))

	yr = request.form['year']
	act = request.form['actor'].title()
	dr = request.form['director'].title()
	r_d = request.form['release_date']
	rt = request.form['rating']

	try:
		cur = cnx.cursor()
		temp = "UPDATE movies SET year = '"  + yr +  "', actor = '" + act + "', director = '" + dr + "', release_date = '" + r_d + "', rating = '" + rt + "' WHERE title = '" + ttl + "'"
		cur.execute(temp)
		cnx.commit()
		return print_msg("Movie " + ttl + " updated successfully")
	except Exception as exp:
		return print_msg("Got exception: " + str(exp))


@app.route('/delete_movie', methods=['POST'])
def delete_movie():
	print("delete_movie request Received")

	db, username, password, hostname = get_db_creds()
	ttl = request.form['delete_title'].title()
	cnx = ''
	try:
		cnx = mysql.connector.connect(user=username, password=password,
									  host=hostname,
									  database=db)
	except Exception as exp:
		print(exp)
		import MySQLdb
		cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

	try:
		cur = cnx.cursor()
		cur.execute("SELECT * FROM movies WHERE title = '" + ttl + "'")
		entries = [dict(movie=row) for row in cur.fetchall()]
		if not len(entries):
			return print_msg("Movie " + ttl + " could not be deleted - Movie does not exist")
	except Exception as exp:
		return print_msg("Got exception: " + str(exp))

	try:
		cur = cnx.cursor()
		temp = "DELETE FROM movies WHERE title = '" + ttl + "'"
		cur.execute(temp)
		cnx.commit()
		return print_msg("Movie " + ttl + " deleted successfully")
	except Exception as exp:
		return print_msg("Got exception: " + str(exp))

@app.route('/search_movie', methods=['GET'])
def search_movie():
	print("search_movie request Received")
	db, username, password, hostname = get_db_creds()
	act = request.args.get('search_actor').title()
	cnx = ''

	try:
		cnx = mysql.connector.connect(user=username, password=password,
									  host=hostname,
									  database=db)
	except Exception as exp:
		print(exp)
		import MySQLdb
		cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

	try:
		cur = cnx.cursor()
		cur.execute("SELECT * FROM movies WHERE actor = '" + act + "'")
		entries = [dict(movie=row) for row in cur.fetchall()]
		if not len(entries):
			return print_msg("No movies with actor " + act + " found in the database")
		else:
			items = []
			for entry in entries:
				temp = "Title: " + entry['movie'][2] + "    Year: " + str(entry['movie'][1]) + "    Director: " + entry['movie'][3] + "    Actor: " + entry['movie'][4] + "    Release Date: " + str(entry['movie'][5]) + "    Rating: " + str(entry['movie'][6])
				items.append(temp)
			print("Movie/s Found")
			return print_entry_msg("Movie/s with actor " + act + "  found." , items)
	except Exception as exp:
		return print_msg("Got exception: " + str(exp))

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
	print("highest_rating request Received")
	db, username, password, hostname = get_db_creds()
	cnx = ''

	try:
		cnx = mysql.connector.connect(user=username, password=password,
									  host=hostname,
									  database=db)
	except Exception as exp:
		print(exp)
		import MySQLdb
		cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

	try:
		cur = cnx.cursor()
		cur.execute("SELECT * FROM movies WHERE rating = (SELECT MAX(rating) FROM movies )")
		entries = [dict(movie=row) for row in cur.fetchall()]
		if not len(entries):
			return print_msg("Could not find highest rating movie - Database is Empty!!!")
		else:
			items = []
			for entry in entries:
				temp = "Title: " + entry['movie'][2] + "    Year: " + str(entry['movie'][1]) + "    Director: " + entry['movie'][3] + "    Actor: " + entry['movie'][4] + "    Release Date: " + str(entry['movie'][5]) + "    Rating: " + str(entry['movie'][6])
				items.append(temp)
			print("Highest rating Movie/s Found")
			return print_entry_msg("Highest Rating Movie/s:" , items)
	except Exception as exp:
		return print_msg("Got exception: " + str(exp))

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
	print("lowest_rating request Received")
	db, username, password, hostname = get_db_creds()
	cnx = ''

	try:
		cnx = mysql.connector.connect(user=username, password=password,
									  host=hostname,
									  database=db)
	except Exception as exp:
		print(exp)
		import MySQLdb
		cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

	try:
		cur = cnx.cursor()
		cur.execute("SELECT * FROM movies WHERE rating = (SELECT MIN(rating) FROM movies)")
		entries = [dict(movie=row) for row in cur.fetchall()]
		if not len(entries):
			return print_msg("Could not find lowest rating movie - Database is Empty!!!")
		else:
			items = []
			for entry in entries:
				temp = "Title: " + entry['movie'][2] + "    Year: " + str(entry['movie'][1]) + "    Director: " + entry['movie'][3] + "    Actor: " + entry['movie'][4] + "    Release Date: " + str(entry['movie'][5]) + "    Rating: " + str(entry['movie'][6])
				items.append(temp)
			print("Lowest Ratings Movie/s Found")
			return print_entry_msg("Lowest Rating Movie/s:" , items)
	except Exception as exp:
		return print_msg("Got exception: " + str(exp))


@app.route("/")
def hello():
	print("Printing available environment variables")
	print(os.environ)
	print("Before displaying index.html")
	#entries = query_data()
	#print("Entries: %s" % entries)
	return render_template('index.html', message= 'Welcome to movie Database')


if __name__ == "__main__":
	app.debug = True
	app.run(host='0.0.0.0')
